<?php


namespace app\controllers;


use app\models\AppModel;
use eldrive\App;
use eldrive\base\Controller;
use eldrive\Cache;


class AppController extends Controller
{
    public function __construct($route)
    {
        parent::__construct($route);
        new AppModel();
        App::$app->setProperty('category_for_menu', self::cacheCategory());
//        debug(App::$app->getProperties()); // печать данных, приходящих на страницу
    }
    public static function cacheCategory(){
        $cache = Cache::instance();
        $cat_menu = $cache->get('category_for_menu');
        if(!$cat_menu){
            $cat_menu = \R::getAssoc("SELECT * FROM category");
            $cache->set('category_for_menu', $cat_menu);
        }
        return $cat_menu;
    }
}