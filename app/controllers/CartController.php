<?php


namespace app\controllers;

use app\models\Cart;


class CartController extends AppController{
    public function viewAction(){

        $this->setMeta('Карточка', '', '');
    }
    public function indexAction(){

    }

    public function addAction(){
        $id = !empty($_GET['id'])?(int)$_GET['id']:null;
        $qty = !empty($_GET['qty'])?(int)$_GET['qty']:null;

        if($id){
            $product = \R::findOne('products', 'id=?', [$id]);
            if(!$product){
                return false;
            }
        }

        $cart = new Cart();
        $cart->addToCart($product, $qty);
        if($this->isAjax()){
            $this->loadView('cart_modal');
        }
        redirect();

    }
    public function deleteAction(){
        $id = !empty($_GET['id']) ? (int)$_GET['id'] : null;
        if(isset($_SESSION['cart'][$id])){
            $cart = new Cart();
            $cart->deleteItem($id);
        }
        if($this->isAjax()){
            $this->loadView('cart_flash');
        }
        redirect();
    }

    public function clearAction(){
        unset($_SESSION['cart']);
        unset($_SESSION['cart.qty']);
        unset($_SESSION['cart.sum']);
        if($this->isAjax()){
            $this->loadView('cart_flash');
        }
        redirect();
    }
    public function purchaseAction(){
        if($_SESSION['user']){
            if($_SESSION['cart']){
                $cart = new Cart();
                $purchase_number = $cart->purshaseOrder();

                unset($_SESSION['cart']);
                $_SESSION['orders'] = $purchase_number;
            }
        }
        if($this->isAjax()){
            $this->loadView('cart_flash');
        }
        redirect();
    }
}