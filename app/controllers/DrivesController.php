<?php


namespace app\controllers;
use app\models\Drives;
use app\models\Motors;
use eldrive\App;

class DrivesController extends AppController {

    public function viewAction(){
        $this->setMeta('Привода', '', '');
        $drives = new Drives();
        $assort_drives = $drives->getDrives();
        $this->set(compact('assort_drives'));
    }

}