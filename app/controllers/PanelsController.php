<?php


namespace app\controllers;
use app\models\Panels;
use eldrive\App;


class PanelsController extends AppController{
    public function viewAction(){
        $this->setMeta('Солнечные панели', '', '');
        $panels = new Panels();
        $assort_panels = $panels->getPanels();
        $this->set(compact('assort_panels'));
    }

}