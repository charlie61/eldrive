<?php


namespace app\controllers;


use app\models\Product;
use eldrive\App;

class ProductController extends AppController{

    public function viewAction(){
        $output = [];
        $alias = $this->route['alias'];
        $product = \R::getCell('SELECT products.product_category
          FROM products
          WHERE products.product_status = 1 and products.product_alias = ?', [$alias]);

        $id_product = \R::getCell('SELECT products.id
          FROM products
          WHERE products.product_status = 1 and products.product_alias = ?', [$alias]);

        if(!$product){
            throw new \Exception('Страница не найдена', 404);
        }
        else{

            if($product==1){
                $get_panel = \R::getAssoc('SELECT products.id, products.product_price, products.product_desc,
                products.product_img, panels.panel_brand_id, panels.panel_voltage, panels.panel_power 
                FROM products LEFT JOIN panels ON panels.panel_product_id = products.id 
                WHERE products.product_alias = ?', [$alias]);
                $output = $get_panel;
            }
            if($product==2){
                $get_motors = \R::getAssoc('SELECT products.id, products.product_price, products.product_desc,
                products.product_img, motors.motor_brand_id, motors.motor_supply, motors.motor_supply, 
                motors.motor_ip, motors.motor_power 
                FROM products LEFT JOIN motors ON motors.motor_product_id = products.id 
                WHERE products.product_alias = ?', [$alias]);
                $output = $get_motors;
            }
            if($product==3){
                $get_drives = \R::getAssoc('SELECT products.id, products.product_price, products.product_desc, 
                products.product_img, drives.drive_brand_id, drives.drive_supply, drives.drive_ip, drives.drive_power 
                FROM products LEFT JOIN drives ON drives.drive_product_id = products.id 
                WHERE products.product_alias = ?', [$alias]);
                $output = $get_drives;
            }

//            array_push($output, $id_product);

                $this->set(compact('output'));
        }
    }
}
