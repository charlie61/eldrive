<?php

namespace app\controllers;


use app\models\Search;

class SearchController extends AppController{
    public function viewAction(){

        $search = new Search();
        $query = $search->validateSearch();
        $this->setMeta('Поиск '.$query);
        $discovered = $search->search();
        $this->set(compact('discovered'));

    }

}