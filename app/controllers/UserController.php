<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 20.02.2019
 * Time: 19:18
 */

namespace app\controllers;
use app\models\User;

class UserController extends AppController
{
        public function indexAction(){

        }

        public function signupAction(){
            if(!empty($_POST)){
                $user = new User();
                $data = $_POST;
                $user->load($data);
                if(!$user->validate($data) || !$user->checkUnique()){
                   $user->getErrors();

                }else{
                    $user->attributes['user_password'] = password_hash($user->attributes['user_password'], PASSWORD_DEFAULT);
                    if ($user->save('user')){
                        $_SESSION['success'] = 'Вы зарегистрированы!';
                    }else{
                        $_SESSION['error'] = 'Ошибка!';
                    }
                }
                redirect();
//                debug($user);
//                die;

            }
            $this->setMeta('Зарегистрироваться');
        }

        public function loginAction(){
            if(!empty($_POST)){
                $user = new User();
                if($user->login()){
                    $_SESSION['success'] = 'Вы успешно авторизованы';
                }else{
                    $_SESSION['error'] = 'Логин/Пароль введены неверно';
                }
                redirect();
            }
            $this->setMeta('Авторизоваться');

        }
        public function logoutAction(){
            if(isset($_SESSION['user'])) unset($_SESSION['user']);
            redirect();

        }

}