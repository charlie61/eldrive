<?php


namespace app\models;


use eldrive\base\Model;


class AppModel extends Model{

    public $errors = [];

    public function validateSearch(){
        if(isset($_GET['find'])){
            $find_raw = strip_tags($_GET['find']);
            if(preg_match("/([-a-zA-Zа-яА-Я0-9+_#\/]{1,20})$/u", $find_raw)){
               $find = $find_raw;
               return $find;
            }else {
                $_GET['find'] = null;
                return false;
            }
        }else{
            $_GET['find'] = null;
            return false;
        }
    }

}