<?php


namespace app\models;
use eldrive\App;
use eldrive\Cache;
use app\models\User;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class Cart extends AppModel{

    public function addToCart($product, $qty = 1){
//        echo 'Cart ';

    $ID = $product->id;
    $title = $product->product_desc;
    $price = $product->product_price;
//    debug($_SESSION);
//    debug($ID);
//    debug($title);
//    debug($price);

        if(isset($_SESSION['cart'][$ID])){
            $_SESSION['cart'][$ID]['qty']+=$qty;
        }else{
            $_SESSION['cart'][$ID] = [
                'qty' => $qty,
                'title' => $title,
                'alias' => $product->product_alias,
                'price' => $price,
                'img' => $product->product_img,
            ];
        }
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty']+$qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum']+$qty*$price : $qty*$price;
    }

    public function deleteItem($id){
        $qtyMinus = $_SESSION['cart'][$id]['qty'];
        $sumMinus = $_SESSION['cart'][$id]['qty'] * $_SESSION['cart'][$id]['price'];
        $_SESSION['cart.qty'] -= $qtyMinus;
        $_SESSION['cart.sum'] -= $sumMinus;
        unset($_SESSION['cart'][$id]);
    }
    public function purshaseOrder(){
        $shipping = \R::dispense('shippings');
        $shipping->shipping_user_id = $_SESSION['user']['id'];
        $shipping_id = \R::store($shipping);
        foreach ($_SESSION['cart'] as $category=>$item){
            $order = \R::dispense('orders');
            $order->order_shipping_id = $shipping_id;
            $order->order_product_id = $category;
            foreach ($item as $key=>$value){
                if($key =='qty'){
                    $order->orders_qty = $value;
                }
                if($key == 'price'){
                    $order->orders_price = $value;
                }
                if($key =='title'){
                    $order->order_title = $value;
                }
            }
            $order_id = \R::store($order);
        }

        $transport = (new Swift_SmtpTransport(App::$app->getProperty('smtp_host'), App::$app->getProperty('smtp_port'), App::$app->getProperty('smtp_protocol')))
            ->setUsername(App::$app->getProperty('smtp_login'))
            ->setPassword(App::$app->getProperty('smtp_password'))
        ;
        $mailer = new Swift_Mailer($transport);

        ob_start();
        require APP . '/views/mail/mail_order.php';
        $body = ob_get_clean();

        $message_client = (new Swift_Message("Заказ № {$shipping_id}"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('shop_name')])
            ->setTo($_SESSION['user']['user_email'])
            ->setBody($body, 'text/html');

        $message_admin = (new Swift_Message("Заказ № {$shipping_id}"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('shop_name')])
            ->setTo(App::$app->getProperty('admin_email'))
            ->setBody($body, 'text/html');

        $result = $mailer->send($message_client);
        $result = $mailer->send($message_admin);

        return $shipping_id;




    }
}