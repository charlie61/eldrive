<?php


namespace app\models;
use eldrive\Cache;

class Drives{
    public function getDrives(){
        $cache = Cache::instance();
        $list_drives = $cache->get('assort_drives');
        if(!$list_drives){
            $list_drives = self::getAvailableDrives();
            $cache->set('assort_drives', $list_drives, 1);
        }
        return $list_drives;
    }

    public function getAvailableDrives(){
        $drives = \R::getAssoc('SELECT products.id, products.product_price,
         products.product_alias, products.product_desc, products.product_img
          FROM products
          WHERE products.product_status = 1 AND products.product_category = 3
          ORDER BY products.id');
        return $drives;
    }


}