<?php


namespace app\models;
use eldrive\Cache;

class Motors{

    public function getMotors(){
        $cache = Cache::instance();
        $list_motors = $cache->get('motors_available');
        if(!$list_motors){
            $list_motors = self::getAvailableMotors();
            $cache->set('motors_available', $list_motors);
        }
        return $list_motors;
    }

    public function getAvailableMotors(){
        $motors = \R::getAssoc('SELECT products.id, products.product_price,
         products.product_alias, products.product_desc, products.product_img
          FROM products
          WHERE products.product_status = 1 AND products.product_category = 2
          ORDER BY products.id');
        return $motors;
    }

}