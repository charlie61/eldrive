<?php

namespace app\models;
use eldrive\Cache;

class Panels{
    public function getPanels(){
        $cache = Cache::instance();
        $list_panels = $cache->get('assort_panels');
        if(!$list_panels){
            $list_panels = self::getAvailablePanels();
            $cache->set('assort_panels', $list_panels, 1);
        }
        return $list_panels;
    }

    public function getAvailablePanels(){
        $panels = \R::getAssoc('SELECT products.id, products.product_price,
         products.product_alias, products.product_desc, products.product_img
          FROM products
          WHERE products.product_status = 1 AND products.product_category = 1
          ORDER BY products.id');
        return $panels;
    }


}