<?php
/**
 * Класс для подготовки данных для страницы Поиск
 */

namespace app\models;


class Search extends AppModel{
    /**
     * Получение необходимых данных для поиска
     * @return array @get_all_founded массив со всеми найденными
     * товарами
     */

    public function search(){
        /**
         * получаем отвалидированное значение введенное в строку поиска
         * методом класса родителя AppModel validateSearch
         */
        $search_string = $this->validateSearch();

        # получаем из базы описания всех продуктов

        $all_available = self::getDesc();

        # преобразовываем строку массив, разбив строку запроса по пробелам (не более пяти)

        $key_words = explode(' ', $search_string, 5);

        # готовим массив в который будем складывать результаты совпадений

        $founded_result = [];

        # перебираем циклами каждое слово в описании товара, сравнивая его со словами запроса

        foreach ($key_words as $item=>$value){
                foreach ($all_available as $k=>$j){
                    if(!empty($value)){

        # при совпадении упаковываем значения id элемента, в котором нашли совпадение

                        $package = mb_stripos($j, $value, 0);
                        if($package!=false or $package===0){
                            array_push($founded_result, $k);
                        }
                    }
                }
        }

        # удаляем из полученного массива дублирующиеся результаты

        $unic_results = array_unique($founded_result);

        # помещаем в массив все полученные товары с отобранными идентификаторами

        $get_all_founded = self::getSome($unic_results);

        return $get_all_founded;
    }

    /**
     * @return array @assort
     */

    # получаем из базы данных список всех доступных товаров с описанием

    public function getDesc(){
        $assort = \R::getAssoc('
                    SELECT products.id, products.product_desc 
                    FROM products 
                    WHERE products.product_status = 1');
        return $assort;
    }

    /**
     * @bunch array передаем массив с id товара, которые надо выбрать
     * @return array @output
     */

    public function getSome($bunch){

    # готовим массив в который будем складывать данные отсортированных товаров

        $output=[];

    # циклом выбираем данные из базы согласно id каждого выбранного товара

        foreach ($bunch as $key=>$value){
            $one = \R::getRow('SELECT products.id,
                    products.product_desc,
                    products.product_category, products.product_alias,
                    products.product_price,
                    products.product_desc, products.product_img        
                    FROM products 
                    WHERE products.id = ?', [$value]);
        array_push($output, $one);
        }

    # @return @output возвращаем отобранный массив с данными товаров

        return $output;
    }
}