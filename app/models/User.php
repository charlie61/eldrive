<?php


namespace app\models;


class User extends AppModel{

    public $attributes = [
        'user_login'=> '',
        'user_password' => '',
        'user_name' => '',
        'user_email' => '',
    ];

    public $rules = [
      'required' => [
          ['user_login'],
          ['user_password'],
          ['user_name'],
          ['user_email'],
      ],
        'email' => [
            ['user_email'],
        ],
        'lengthMin' => [
          ['user_password', 6],
        ]
    ];

    public function checkUnique(){
        $user = \R::findOne('user', 'user_login = ?', [$this->attributes['user_login']]);
        if($user){
            if($user->user_login == $this->attributes['user_login']){
                $this->errors['unique'][] = 'Этот логин уже занят';
            }
            return false;
        }
        return true;
    }

    public function login($isAdmin = false){
        $user_login = !empty(trim($_POST['user_login'])) ? trim($_POST['user_login']) : null;
        $user_password = !empty(trim($_POST['user_password'])) ? trim($_POST['user_password']) : null;
        if($user_login && $user_password){
            if($isAdmin){
                $user = \R::findOne('user', "user_login = ? AND user_role = 'admin'", [$user_login]);
            }else{
                $user = \R::findOne('user', "user_login = ?", [$user_login]);
            }
            if($user){
                if(password_verify($user_password, $user->user_password)){
                    foreach($user as $k => $v){
                        if($k != 'user_password') $_SESSION['user'][$k] = $v;
                    }
                    return true;
                }
            }
        }
        return false;
    }
    public static function checkAuth(){
        return isset($_SESSION['user']);
    }

    public static function isAdmin(){
        return (isset($_SESSION['user']) && $_SESSION['user']['user_role'] == 'admin');
    }


}