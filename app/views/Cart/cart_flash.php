            <?php
            if(!empty($_SESSION['cart'])):?>
                <div class="table-responsive" style="max-width: 700px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Фото</th>
                            <th>Наименование</th>
                            <th>Количество</th>
                            <th>Цена</th>
                            <th><i class="fa fa-times" aria-hidden="true"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($_SESSION['cart'] as $id => $item): ?>
                            <tr>
                                <td><a href="product/<?=$item['alias']; ?>"><img src="img/<?=$item['img']; ?>" alt="" class="img-fluid" style="max-width: 150px;"></a></td>
                                <td><?=$item['title']; ?></td>
                                <td><?=$item['qty']; ?></td>
                                <td><?=$item['price']; ?></td>
                                <td><a href="#" class="del-item" data-id="<?=$id; ?>"><i class="fa fa-times text-danger" aria-hidden="true"  style="font-size: 1.5em;"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td>Итого: </td>
                            <td colspan="4" class="text-right cart-qty"><?=$_SESSION['cart.qty']; ?></td>
                        </tr>
                        <tr>
                            <td>На сумму: </td>
                            <td colspan="4" class="text-right cart-sum"><?=$_SESSION['cart.sum']; ?> рублей</td>
                        </tr>
                        </tbody>
                    </table>
                    <div>
                        <div style="display: inline-block">
                            <a href="#" class="btn btn-light btn-card" id="clearAll">Очистить все</a>
                        </div>
                        <?php
                        if(!empty($_SESSION['user'])):?>
                            <div style="display: inline-block">
                                <a href="cart/purchase" class="btn btn-light btn-card" id="purchase">Заказать</a>
                            </div>
                        <?php else: ?>
                            <div style="display: inline-block">
                                <a href="user/login" class="btn btn-light btn-card">Авторизоваться</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php else: ?>
               <div >
                   <div class="row justify-content-center text-center main rounded">
                       <h3>Корзина пуста</h3>
                   </div>
                   <div class="row justify-content-center text-center main rounded">
                       <?php if(!empty($_SESSION['orders']) and !empty($_SESSION['cart.sum'])):?>
                           <div class="row justify-content-center text-center main rounded">
                               <div>
                                   Ваш заказ номер <?= $_SESSION['orders'];?> на сумму <?= $_SESSION['cart.sum']; ?> рублей принят!
                               </div>
                               <?php
                               unset($_SESSION['cart.qty']);
                               unset($_SESSION['orders']);
                               unset($_SESSION['cart.sum']);?>
                           </div>
                       <?php endif; ?>
                   </div>
               </div>

            <?php endif; ?>
