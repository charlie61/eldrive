index
<div class="container justify-content-center">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-center">
        <div class="row justify-content-center text-center main rounded" id="cart-controller"  style="min-height: 400px; margin-top: 30px; padding-top: 20px; margin-bottom: 30px; padding-bottom:20px;">
<?php
    if(!empty($_SESSION['cart'])):?>
        <div class="table-responsive" style="max-width: 700px;">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Фото</th>
                    <th>Наименование</th>
                    <th>Количество</th>
                    <th>Цена</th>
                    <th><i class="fa fa-times" aria-hidden="true"></i></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($_SESSION['cart'] as $id => $item): ?>
                    <tr>
                        <td><a href="product/<?=$item['alias']; ?>"><img src="img/<?=$item['img']; ?>" alt="" class="img-fluid" style="max-width: 150px;"></a></td>
                        <td><?=$item['title']; ?></td>
                        <td><?=$item['qty']; ?></td>
                        <td><?=$item['price']; ?></td>
                        <td><a href="#" class="del-item" data-id="<?=$id; ?>"><i class="fa fa-times text-danger" aria-hidden="true"  style="font-size: 1.5em;"></i></a></td>
                    </tr>
                <?php endforeach; ?>
                    <tr>
                        <td>Итого: </td>
                        <td colspan="4" class="text-right cart-qty"><?=$_SESSION['cart.qty']; ?></td>
                    </tr>
                    <tr>
                        <td>На сумму: </td>
                        <td colspan="4" class="text-right cart-sum"><?=$_SESSION['cart.sum']; ?> рублей</td>
                    </tr>
                </tbody>
            </table>
            <div>
                <div style="display: inline-block">
                    <a href="#" class="btn btn-light btn-card" id="clearAll">Очистить все</a>
                </div>
                <div style="display: inline-block">
                    <a href="cart/view" class="btn btn-light btn-card">Заказать</a>
                </div>
            </div>
            
        </div>
<?php else: ?>
    <h3>Корзина пуста</h3>
<?php endif; ?>
            <?php if(!empty($_SESSION['orders']) and !empty($_SESSION['cart.sum'])):?>
                <div class="row justify-content-center text-center main rounded">
                    <div>
                        Ваш заказ номер <?= $_SESSION['orders'];?> на сумму <?= $_SESSION['cart.sum']; ?> рублей принят!
                    </div>
<!--                    --><?php //unset($_SESSION['cart.qty']);
//                    unset($_SESSION['cart.sum']);?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

