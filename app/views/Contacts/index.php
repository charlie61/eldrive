<div id="map"></div>
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-center">
        <div class="row justify-content-center text-center">
            <form action="" class="form-signin" style="width: 300px; min-height: 500px;">
                 <h1 class="h3 mb-3 font-weght-normal">Напишите нам:</h1>

                <input type="text" id="inputTheme" class="form-control" placeholder="Тема письма" required="" style="">

                <textarea id="inputText" class="form-control" placeholder="Ваше сообщение.." required="" style="margin-top: 20px;"></textarea>

                <button class="btn btn-light btn-card" type="submit" style="margin-top: 20px;">Отправить</button>
                <p class="mt-5 mb-3 text-muted">© 2019-2020</p>
            </form>
        </div>
    </div>
</div>
<script type='text/javascript'>
    var map = L.map('map').setView([60.006278, 30.37946], 15);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    let markers = [];

    function createMarker(coords) {
        let id;
        if (markers.length < 1) id = 0;
        else id = markers[markers.length - 1]._id + 1;
        var popupContent =
            '<p>Some Infomation</p></br>' +
            '<p>test</p></br>' +
            '<button onclick="clearMarker(' + id + ')">Clear Marker</button>';
        myMarker = L.marker(coords, {
            draggable: false
        });
        myMarker._id = id;
        var myPopup = myMarker.bindPopup(popupContent, {
            closeButton: true
        });
        map.addLayer(myMarker);
        markers.push(myMarker);
    }

    function clearMarker(id) {
        console.log(markers);
        let new_markers = [];
        markers.forEach(function(marker) {
            if (marker._id == id) map.removeLayer(marker);
            else new_markers.push(marker)
        });
        markers = new_markers
    }

    function onMapClick(e) {
        createMarker(e.latlng);

    }
    map.on('click', onMapClick);


</script>