
<!--слайдер-->
    <div class="slide-one-item home-slider owl-carousel" id="slide">
        <div class="site-blocks-cover overlay" id="slide1" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-md-10">
                        <h1 class="text-white">Комплексные решения</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="site-blocks-cover overlay" id="slide2" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-md-10">
                        <h1 class="text-white">Лучшие специалисты</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="site-blocks-cover overlay " id="slide3" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row  justify-content-center text-center">
                    <div class="col-md-10">
                        <h1 class="text-white">Экологичность</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="site-blocks-cover overlay" id="slide4" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-md-10">
                        <h1 class="text-white">Экономичность</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--слайдер-->

<div class="notation">
    <div class="container-fluid">
        <div class="row main justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xl-10 annotation" data-aos="fade-up">
                <p class="lead">ElectroDrive объединяет все лучшие достижения в электротехнической инженерии. Здесь Вы найдете все необходимое для уверенного
                    роста ваших инжиниринговых возможностей.</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xl-10 annotation" data-aos="fade-up">
                <div class="row opportunity d-sm-block justify-content-center">
                    <div class="card-deck justify-content-center">
                        <div class="card text-center" data-aos="fade-up">
                            <div class="card-body">
                                <h5 class="card-title">Разработка</h5>
                            </div>
                            <img src="img/develop.jpg" class="card-img-bottom" alt="...">
                        </div>
                        <div class="card text-center" data-aos="fade-up">
                            <div class="card-body">
                                <h5 class="card-title">Современное оборудование</h5>
                            </div>
                            <img src="img/ABB-drives.jpg" class="card-img-bottom" alt="...">
                        </div>
                        <div class="card text-center" data-aos="fade-up">
                            <div class="card-body">
                                <h5 class="card-title">Рост доходности</h5>
                            </div>
                            <img src="img/grow.jpg" class="card-img-bottom" alt="...">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if($main_category):?>
<div class="brands">
    <div class="container-fluid justify-content-center">
        <div class="row main justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bests justify-content-center" data-aos="fade-up">
                <div class="row justify-content-center text-center">
                    <div class="card-deck justify-content-center">
                        <?php foreach ($main_category as $category):?>
                            <div class="card text-center justify-content-center" data-aos="fade-up">
                                <img src="img/<?=$category->category_img;?>" class="img-fluid card-img-bottom" alt="...">
                                <div class="card-body">
                                </div>
                                <div class="card-footer">
                                    <h5 class="card-title"><?=$category->category_title;?></h5>
                                    <a href="<?=$category->category_alias;?>" class="btn btn-light btn-card">Купить</a>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php endif;?>
<div>
    <div class="justify-content-center text-center partners">
        <div class="row justify-content-center text-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-xl-10 logos justify-content-center">
                <div class="row justify-content-center text-center">
                    <div class="card-deck text-center justify-content-center">
                        <div class="card" data-aos="zoom-in">
                            <img src="img/weiland.jpg" class="card-img-top" alt="...">
                        </div>
                        <div class="card" data-aos="zoom-in">
                            <img src="img/OCP.jpg" class="card-img-top" alt="...">
                        </div>
                        <div class="card" data-aos="zoom-in">
                            <img src="img/siemens-Logo.png" class="card-img-top" alt="...">
                        </div>
                        <div class="card" data-aos="zoom-in">
                            <img src="img/general-electric.png" class="card-img-top" alt="...">
                        </div>
                        <div class="card" data-aos="zoom-in">
                            <img src="img/CYBERDYNE.png" class="card-img-top" alt="...">
                        </div>
                        <div class="card" data-aos="zoom-in">
                            <img src="img/abb.jpg" class="card-img-top" alt="...">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
