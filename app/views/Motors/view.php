<div class="container main-content">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-center">
    <div class="row justify-content-center text-center">
        <div class="main-content">
            <h5>У нас вы можете приобрести:</h5>
        </div>
    </div>
    <div class="row justify-content-center text-center">
        <div class="card-deck justify-content-center text-center">
            <?php
            if ($assort_motors){
                foreach ($assort_motors as $key=>$value){
                    echo '<div class="card text-center card-category">
                                 <img src="img/';
                    foreach ($value as $category=>$item){
                        if($category=='product_img')
                            echo $item;
                    }
                    echo '" class="card-img-top img-fluid" alt="...">
                                      <div class="card-body">
                                        <h6 class="card-title">';
                    foreach ($value as $category=>$item){
                        if($category=='product_price')
                            echo round($item);
                    }
                    echo ' рублей</h6>
                                        <p class="card-text text-left">';
                    foreach ($value as $category=>$item){
                        if($category=='product_desc')
                            echo $item;
                    }
                    echo '</p>                                    
                                    </div>
                                      <div class="card-footer">
                                        <a href="product/';
                    foreach ($value as $category=>$item){
                        if($category=='product_alias')
                            echo $item;
                    }
                    echo  '" class="btn btn-light btn-card">Купить</a>
                                      </div>
                              </div>';

                }
            }else{
                echo '<h3>Ничего не найдено.</h3>';
            }
            ?>
        </div>
    </div>
</div>
</div>
