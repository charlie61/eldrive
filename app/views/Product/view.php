<?php
//debug($output);
//debug($_SESSION['cart']);
?>

<div class="container">
    <div>
        <h5>Описание:</h5>
    </div>
</div>
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-center">
        <div class="row justify-content-center text-center jumbotron">
           <div class="rounded" style="max-width: 400px;">
               <div class="text-left">
               <?php
                   foreach ($output as $category=>$item){
                           foreach ($item as $key=>$value){
                               if($key==='product_desc'){
                                   echo '<div>
                                            <p>'.$value.'</p>
                                        </div>';
                               }
                               if($key==='drive_supply'){
                                   echo '<div>
                                           <p>Напряжение питания: '.$value.' Вольт</p>
                                       </div>';
                               }
                               if($key==='motor_supply'){
                                   echo '<div>
                                           <p>Напряжение питания: '.$value.' Вольт</p>
                                       </div>';
                               }
                               if($key==='drive_ip'){
                                   echo '<div>
                                           <p>IP: '.$value. '</p>
                                       </div>';
                               }
                               if($key==='motor_ip'){
                                   echo '<div>
                                           <p>IP: '.$value. '</p>
                                       </div>';
                               }
                               if($key==='panel_voltage'){
                                   echo '<div>
                                           <p>Номинальное напряжение : '.$value. ' Вольт</p>
                                       </div>';
                               }
                               if($key==='drive_power'){
                                   echo '<div>
                                       <p>Мощность: '.$value.' кВт</p>
                                   </div>';
                               }
                               if($key==='motor_power'){
                                   echo '<div>
                                       <p>Мощность: '.$value.' кВт</p>
                                   </div>';
                               }
                               if($key==='panel_power'){
                                   echo '<div>
                                       <p>Номинальная мощность: '.round($value).' Вт</p>
                                   </div>';
                               }
                               if($key==='product_price'){
                                   echo '<div>
                                       <h5>Цена: '.round($value).' Руб.</h5>
                                   </div>
                                 ';
                               }

                           }

                   }
                   echo '</div>';
                            echo '</div>
                                <div class="" style="display: inline-block;  max-width: 180px; margin-left: 20px;" >';
                   foreach ($output as $category=>$item) {
                       if (!is_numeric($item)) {
                           foreach ($item as $key => $value){
                               if($key==='product_img'){
                                   echo ' <img src="img/'.$value.'" alt="" class="img-fluid">';
                               }
                           }
                       }
                   }
                   echo '<div>
                            <input type="number" size="4" value="1" name="quantity" min="1" step="1" class="form-control" id="quantity">
                         </div>';
                   foreach ($output as $category=>$item){
                       echo '<a href="cart/add?id='.$category.'" class="btn btn-light btn-card" id="add-to-cart" data-id="'.$category.'">В корзину</a>';
                   }
               ?>
               </div>
            </div>
        </div>
    </div>
</div>
