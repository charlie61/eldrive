<div class="container main-content" style="min-height: 65vh;">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-center">
    <div class="row justify-content-center text-center">
        <div class="main-content">
            <?php
            if($discovered) {
                echo '<h4>Результаты поиска:</h4>';
            }else{
                echo '<h5>Ничего не найдено..</h5>';
            }
            ?>
        </div>
    </div>
    <div class="row justify-content-center text-center">
        <div class="card-deck justify-content-center text-center" style="min-height:">
            <?php
            if ($discovered){
                foreach ($discovered as $key=>$value){
                        echo '<div class="card text-center card-category">
                                 <img src="img/';
                                    foreach ($value as $category=>$item){
                                        if($category=='product_img')
                                            echo $item;
                                    }
                        echo '" class="card-img-top img-fluid" alt="...">
                                      <div class="card-body">
                                        <h5 class="card-title">';
                                        foreach ($value as $category=>$item){
                                            if($category=='product_price')
                                                echo $item;
                                        }
                                echo ' рублей</h5>
                                        <p class="card-text text-left">';
                                            foreach ($value as $category=>$item){
                                                if($category=='product_desc')
                                                    echo $item;
                                            }
                                echo '</p>                                    
                                    </div>
                                      <div class="card-footer">
                                        <a href="product/';
                                            foreach ($value as $category=>$item){
                                                if($category=='product_alias')
                                                    echo $item;
                                            }
                                        echo  '" class="btn btn-light btn-card">Купить</a>
                                      </div>
                              </div>';

                }
            }
            ?>
            </div>

        </div>
    </div>
</div>
</div>
