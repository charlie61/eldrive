<div class="container" style="padding-top: 30px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-center">
        <div class="row justify-content-center text-center">
            <form action="user/login" method="post" class="form-signin needs-validation" novalidate style="width: 300px;">
                <img src="img\Logo1.png" class="mb-4 img-fluid" alt="" style="max-width: 180px;">
                <h1 class="h3 mb-3 font-weght-normal">Пожалуйста, авторизируйтесь</h1>
                <div class="form-group">
                    <input type="text" id="inputLogin" class="form-control" placeholder="Ваш логин" pattern="^([-a-zA-Z0-9_$]{4,15})$" maxlength="15" required name="user_login" style="margin-top: 20px;">
                    <div class="invalid-feedback">
                        От 4 до 15 символов. Допустимы латинские заглавные и строчные буквы, цифры, символы _  $ -
                    </div>
                </div>

                <div>
                    <input type="password" id="inputPassword" class="form-control" placeholder="Пароль" pattern="^([-a-zA-Z0-9_@!$]{6,15})$" maxlength="15" name="user_password" required style="margin-top: 20px;">
                    <div class="invalid-feedback">
                        Длинной не менее 6 символов. Допустимы латинские заглавные и строчные буквы, цифры, символы _  $ - @ !
                    </div>
                </div>
                <button class="btn btn-light btn-card" type="submit" style="margin-top: 20px; margin-bottom: 30px;">Авторизоваться</button>
                <a href="user/signup">Зарегистрироваться</a>
                <p class="mt-5 mb-3 text-muted">© 2019-2020</p>
            </form>
        </div>
    </div>
</div>
