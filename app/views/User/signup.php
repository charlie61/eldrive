<div class="container" style="padding-top: 30px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-center">
        <div class="row justify-content-center text-center">
            <form action="user/signup" method="post" id="signup" role="form" class="form-signin needs-validation" novalidate style="width: 300px;">
                <img src="img\Logo1.png" class="mb-4 img-fluid" alt="" style="max-width: 180px;">
                <h2 class="h4 mb-3 font-weght-normal">Пожалуйста, зарегистрируйтесь</h2>
                <div class="form-group">
                    <input type="text" id="inputName" class="form-control" placeholder="Ваше имя" required name="user_name" pattern="^([-a-zA-Zа-яА-Я0-9+_#$]{1,15})$" maxlength="15">
                    <div class="invalid-feedback">
                        Введите Имя.
                    </div>
                </div>

                <div class="form-group">
                    <input type="text" id="inputLogin" class="form-control" placeholder="Ваш логин" pattern="^([-a-zA-Z0-9_$]{4,15})$" maxlength="15" required name="user_login" style="margin-top: 20px;">
                    <div class="invalid-feedback">
                        От 4 до 15 символов. Допустимы латинские заглавные и строчные буквы, цифры, символы _  $ -.
                    </div>
                </div>

                <div class="form-group">
                    <input type="email" id="inputEmail" class="form-control" placeholder="Ваш e-mail" required="required" name="user_email" style="margin-top: 20px;">
                    <div class="invalid-feedback">
                        Введите ваш email.
                    </div>
                </div>

                <div class="form-group">
                    <input type="password" id="inputPassword" class="form-control" placeholder="Пароль" required="required" pattern="^([-a-zA-Z0-9_@!$]{6,15})$" maxlength="15" name="user_password" style="margin-top: 20px;">
                    <div class="invalid-feedback">
                        Длинной не менее 6 символов. Допустимы латинские заглавные и строчные буквы, цифры, символы _  $ - @ !
                    </div>
                </div>
                                <div class="checkbox mb-3 " style="padding-top: 20px;">
                                    <label>
                                        <div class="form-group">
                                            <input type="checkbox" value="remember-me" required="required"><a href="#" style="color: gray"> Я согласен с пользовательским соглашением</a>
                                        </div>
                                    </label>
                                </div>
                <button class="btn btn-light btn-card" type="submit" style="margin-top: 20px;">Зарегистрироваться</button>
                <p class="mt-5 mb-3 text-muted">© 2019-2020</p>
            </form>
        </div>
    </div>
</div>