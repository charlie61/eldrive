<?php
if(isset($_SESSION['cart.qty'])){
    if($_SESSION['cart.qty']==0){
        $cart_info= '';
    }else {
        $cart_info = $_SESSION['cart.qty'];
    }
}else {
    $cart_info= '';
}
//session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?=$this->getMeta();?>
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="lib/owl-carusel/owl.carousel.min.css">
    <link rel="stylesheet" href="lib/owl-carusel/owl.theme.default.min.css">
    <link rel="stylesheet" href="lib/aos-master/aos.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
            integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
            crossorigin=""></script>
    <style>
        #map{
            width: 100%;
            height: 60vh;
            margin-top: 50px;
            margin-bottom: 50px;
        }
    </style>

</head>
<body>
<div class="wrap">
    <div class="container-fluid">
    <div class="row top  justify-content-center">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xs-4 text-center">
            <a class="btn text-dark disabled" href="/#"><i class="fa fa-pencil" aria-hidden="true"></i>E-mail: eldrive@mail.com</a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 text-center">
            <a class="btn text-dark disabled" href="/#"><i class="fa fa-phone" aria-hidden="true"></i>Телефон: +7(812)123-456-78</a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2 col-xl-2 text-center">
            <a class="btn text-dark" href="cart/view"><i class="fa fa-shopping-basket" aria-hidden="true"><span id="cart_info"><?=$cart_info;?></span></i>Корзина</a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2 col-xl-2 text-center">

            <?php if(!empty($_SESSION['user'])): ?>
            <a class="btn text-dark" href="user/logout"><i class="fa fa-sign-in" aria-hidden="true"></i>Выйти</a>

            <?php else: ?>
                <a class="btn text-dark" href="user/login"><i class="fa fa-sign-in" aria-hidden="true"></i>Войти</a>
            <?php endif; ?>

        </div>
    </div>
    </div>
    <div class="menu">
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3edf0;">
            <a class="navbar-brand" href="/">
                <img id="logo" src="img/Logo1.png" width="120" alt="">
            </a>
            <a class="navbar-brand" href="/">Главная</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <?php new \app\widgets\menu\Menu([
                    'tpl' => WWW . '/menu/menu.php',
                ]); ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="contacts">Контакты<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0" method="get" action="search" autocomplete="off">
                    <input class="form-control mr-sm-2 typeahead" id="typeahead" type="text" name="find" placeholder="Поиск" aria-label="Поиск">
                    <button class="btn btn-light my-2 my-sm-0" type="submit">Поиск</button>
                </form>
            </div>
        </nav>

    </div>
<?php if(isset($_SESSION['error'])): ?>
    <div class="container-fluid" id="registry">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-12 text-center">
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset($_SESSION['error']); ?>
                </div>

            </div>
        </div>
    </div>
<?php endif ?>
<?php if(isset($_SESSION['success'])):?>
    <div class="container-fluid" id="registry">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-12 text-center">
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset($_SESSION['success']); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
    <?=$content;?>
<div>
    <div class="container-fluid">
        <div class="row bikini-bottom  justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xs-3 text-center">
                <div class="row justify-content-center">
                    <a class="btn text-dark" href="/#"><i class="fa fa-vk" aria-hidden="true"></i>ВКонтакте</a>
                </div>
                <div class="row justify-content-center">
                    <a class="btn text-dark" href="/#"><i class="fa fa-youtube" aria-hidden="true"></i>YouTube</a>
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 text-center">
                <div class="row justify-content-around">
                    <div class="text-left"><p class="bikini-text">Фактический адрес:</p></div>
                </div>
                <div class="row justify-content-around">
                    <div class="text-left"><p class="bikini-text">Санкт-Петербург, Пекарей ул. 221 лит. Б. </p></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 text-center">
                <div class="row justify-content-around">
                    <div class="text-left"><p class="bikini-text">Администрация:</p></div>
                </div>
                <div class="row justify-content-around">
                    <div class="text-left"><p class="bikini-text">8(812)-123-45-67</p></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<script>
    var path = '<?=PATH;?>'

</script>
<script src="lib/jquery/jquery-3.3.1.min.js"></script>
<script src="lib/migrate/jquery-migrate-3.0.1.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.min.js"></script>
<script src="lib/owl-carusel/owl.carousel.min.js"></script>
<script src="lib/stellar/jquery.stellar.min.js"></script>
<script src="lib/typeahead/typeahead.bundle.js"></script>
<script src="lib/aos-master/aos.js"></script>
<script src="js/app.js"></script>


</body>
</html>