<?php

namespace app\widgets\menu;

use eldrive\App;
use eldrive\Cache;
use RedUNIT\Base\Threeway;

class Menu{

    protected $data;
    protected $menuHtml; // готовый код нашего меню
    protected $tpl;// шаблон для менюшки, подключается из drive.php
    protected $sorted = [];
    protected $tree;
    protected $container = 'ul';
    protected $class = 'menu';
    protected $table = 'category'; // таблица из которой выбираются данные
    protected $cache = 3600; //3600
    protected $cacheKey = 'eldrive_menu';
    protected $attrs = []; // массив дополнительных атрибутов
    protected $prepend = '';

    public function __construct($options = []){
        $this->tpl = __DIR__ . '/menu_tpl/menu.php';
//        debug($options);
        $this->getOptions($options);
        $this->run();
    }

    protected function getOptions($options){
        foreach($options as $k => $v){
            if(property_exists($this, $k)){
                $this->$k = $v;
            }
        }
    }

    protected function run(){
        $cache = Cache::instance();
        $this->menuHtml = $cache->get($this->cacheKey);
        if(!$this->menuHtml){
            $this->data = App::$app->getProperty('category_for_menu');
            if(!$this->data){
                $this->data = $cats = \R::getAssoc("SELECT * FROM {$this->table}");
            }

        }

        $this->output($this->data);
    }

    protected function output($arr)
    {
        $sorted = $this->sorted;
        foreach ($arr as $key => $value) {
            if ($value['category_parent_id'] == '0') {
                array_push($sorted, $value);
            }
        }
        $this->menuHtml = $this->getMenuHtml($sorted);
        echo $this->menuHtml;

    }


    protected function getMenuHtml($arr_menu){
        $str = '';
        ob_start();
        require $this->tpl;
        $str .= ob_get_clean();
        return $str;
    }
}