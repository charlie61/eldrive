<?php

use eldrive\Router;


Router::add('^search', ['controller'=>'Search', 'action'=>'view']);
Router::add('^drives$', ['controller'=>'Drives', 'action'=>'view']);
Router::add('^panels$', ['controller'=>'Panels', 'action'=>'view']);
Router::add('^motors$', ['controller'=>'Motors', 'action'=>'view']);
Router::add('^product/(?P<alias>[a-z0-9-]+)/?$', ['controller' => 'Product', 'action' => 'view']);
// default routes
Router::add('^admin$', ['controller'=>'Main', 'action'=>'index', 'prefix'=>'admin']);
Router::add('^admin/?(?P<conroller>[a-z-]+)/?(?P<action>[a-z-]+)?$', ['prefix'=>'admin']);

Router::add('^$', ['controller'=>'Main', 'action'=>'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');

