-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 26 2019 г., 20:00
-- Версия сервера: 5.6.41
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `eldrive33`
--

-- --------------------------------------------------------

--
-- Структура таблицы `brand`
--

CREATE TABLE `brand` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `brand_title` varchar(255) NOT NULL,
  `brand_alias` varchar(60) NOT NULL,
  `brand_desc` varchar(255) NOT NULL,
  `brand_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `brand`
--

INSERT INTO `brand` (`id`, `brand_title`, `brand_alias`, `brand_desc`, `brand_img`) VALUES
(1, 'ABB', 'abb', 'Мировой производитель электрооборудования', 'abb.jpg'),
(2, 'SIEMENS', 'siemens', 'Мировой производитель электрооборудования', 'siemens-Logo.png'),
(4, 'Hevel', 'hevel', 'Крупнейший производитель солнечных панелей', 'hevel.jpg'),
(5, 'Perlight', 'perlight', 'Крупнейший производитель солнечных панелей', 'perlight.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `category_title` varchar(255) NOT NULL,
  `category_alias` varchar(60) NOT NULL,
  `category_parent_id` tinyint(3) UNSIGNED NOT NULL,
  `category_keywords` varchar(255) DEFAULT NULL,
  `category_description` varchar(255) DEFAULT NULL,
  `category_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `category_title`, `category_alias`, `category_parent_id`, `category_keywords`, `category_description`, `category_img`) VALUES
(1, 'Солнечные панели', 'panels', 0, 'solar', 'Cолнечные панели и оборудование', 'solar-panels.png'),
(2, 'Электродвигатели', 'motors', 0, 'motors', 'Электродвигатели и комплектующие', 'partners-infobaza-files-images_videos-products-jpeg-60c47409abd1feb019d779bc0a64d8dd-auto_width_1000.png'),
(3, 'Привода', 'drives', 0, 'drives', 'Частотные преобразователи и комплектующие', 'ustr-avt-regulir-acs800-01-0060-3-ip21.png');

-- --------------------------------------------------------

--
-- Структура таблицы `connects`
--

CREATE TABLE `connects` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `connect_sessions` char(64) NOT NULL,
  `connect_token` char(32) NOT NULL,
  `connect_user_id` mediumint(8) UNSIGNED NOT NULL,
  `connect_token_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `drives`
--

CREATE TABLE `drives` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `drive_product_id` mediumint(8) UNSIGNED NOT NULL,
  `drive_brand_id` tinyint(3) UNSIGNED NOT NULL,
  `drive_supply` enum('220','380','690','500') NOT NULL,
  `drive_ip` tinyint(3) NOT NULL,
  `drive_power` float(5,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `drives`
--

INSERT INTO `drives` (`id`, `drive_product_id`, `drive_brand_id`, `drive_supply`, `drive_ip`, `drive_power`) VALUES
(1, 9, 1, '380', 21, 132.00),
(2, 10, 1, '220', 20, 0.37),
(3, 11, 2, '380', 20, 5.50),
(4, 12, 2, '380', 22, 0.75);

-- --------------------------------------------------------

--
-- Структура таблицы `motors`
--

CREATE TABLE `motors` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `motor_product_id` mediumint(8) NOT NULL,
  `motor_brand_id` tinyint(3) UNSIGNED NOT NULL,
  `motor_supply` enum('220','380','690','500') NOT NULL,
  `motor_ip` tinyint(3) NOT NULL,
  `motor_power` float(5,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `motors`
--

INSERT INTO `motors` (`id`, `motor_product_id`, `motor_brand_id`, `motor_supply`, `motor_ip`, `motor_power`) VALUES
(1, 1, 1, '380', 54, 18.00),
(2, 2, 1, '380', 55, 37.00),
(3, 3, 2, '380', 55, 3.00),
(4, 4, 2, '690', 65, 20.00),
(5, 5, 1, '500', 56, 50.00),
(6, 6, 1, '500', 65, 160.00),
(7, 7, 2, '500', 55, 15.00),
(8, 8, 2, '500', 56, 13.60);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_shipping_id` int(10) UNSIGNED NOT NULL,
  `order_product_id` mediumint(8) UNSIGNED NOT NULL,
  `orders_qty` smallint(5) UNSIGNED NOT NULL,
  `orders_price` float(10,2) UNSIGNED NOT NULL,
  `order_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `order_shipping_id`, `order_product_id`, `orders_qty`, `orders_price`, `order_title`) VALUES
(1, 2, 3, 1, 120345.00, '1LF7106-2AB 2870 об\\мин'),
(2, 2, 10, 2, 15000.00, 'ABB Устр-во автомат. регулирования ACS55-01E-02A2-2 0,37 кВт 220 В 1 фаза IP20 с фильтром ЭМС '),
(3, 3, 3, 1, 120345.00, '1LF7106-2AB 2870 об\\мин'),
(4, 3, 10, 3, 15000.00, 'ABB Устр-во автомат. регулирования ACS55-01E-02A2-2 0,37 кВт 220 В 1 фаза IP20 с фильтром ЭМС '),
(5, 3, 14, 4, 1810.00, 'Солнечный модуль FSM 30M'),
(6, 3, 2, 1, 381777.00, 'M2AA,IE1,37кВт,1000о/м,IMB3 /1шт./ 3GAA253041-ADG0..'),
(7, 4, 1, 1, 302860.69, 'M2AA 180 MLA IE1, 1500о/м, IMB35 на лапах'),
(8, 5, 6, 1, 22356.00, 'DMR 160 SN'),
(9, 5, 9, 1, 430300.00, 'Преобразователь частоты ACS880-01-246A-3+E200 IP21 132 кВт ЕМС-фильтр лаковое покрытие плат'),
(10, 6, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(11, 7, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(12, 8, 10, 1, 15000.00, 'ABB Устр-во автомат. регулирования ACS55-01E-02A2-2 0,37 кВт 220 В 1 фаза IP20 с фильтром ЭМС '),
(13, 9, 3, 1, 120345.00, '1LF7106-2AB 2870 об\\мин'),
(14, 9, 10, 1, 15000.00, 'ABB Устр-во автомат. регулирования ACS55-01E-02A2-2 0,37 кВт 220 В 1 фаза IP20 с фильтром ЭМС '),
(15, 10, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(16, 11, 9, 1, 430300.00, 'Преобразователь частоты ACS880-01-246A-3+E200 IP21 132 кВт ЕМС-фильтр лаковое покрытие плат'),
(17, 11, 4, 1, 140540.00, '1LF7096-4AB 2820 об\\мин'),
(18, 12, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(19, 13, 10, 1, 15000.00, 'ABB Устр-во автомат. регулирования ACS55-01E-02A2-2 0,37 кВт 220 В 1 фаза IP20 с фильтром ЭМС '),
(20, 14, 9, 1, 430300.00, 'Преобразователь частоты ACS880-01-246A-3+E200 IP21 132 кВт ЕМС-фильтр лаковое покрытие плат'),
(21, 15, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(22, 16, 2, 1, 381777.00, 'M2AA,IE1,37кВт,1000о/м,IMB3 /1шт./ 3GAA253041-ADG0..'),
(23, 17, 9, 1, 430300.00, 'Преобразователь частоты ACS880-01-246A-3+E200 IP21 132 кВт ЕМС-фильтр лаковое покрытие плат'),
(24, 18, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(25, 19, 2, 1, 381777.00, 'M2AA,IE1,37кВт,1000о/м,IMB3 /1шт./ 3GAA253041-ADG0..'),
(26, 20, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(27, 21, 16, 1, 9500.00, 'Солнечные панели DELTA являются фотоэлектрическими модулями'),
(28, 22, 16, 1, 9500.00, 'Солнечные панели DELTA являются фотоэлектрическими модулями'),
(29, 23, 2, 1, 381777.00, 'M2AA,IE1,37кВт,1000о/м,IMB3 /1шт./ 3GAA253041-ADG0..'),
(30, 24, 5, 1, 45567.00, 'DMR 132 KN'),
(31, 24, 11, 1, 81726.00, 'Преобразователь частоты SINAMICS G120 силовой модуль PM230 без фильтра IP20 3AC380-480В 5.5кВт'),
(32, 25, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(33, 25, 2, 1, 381777.00, 'M2AA,IE1,37кВт,1000о/м,IMB3 /1шт./ 3GAA253041-ADG0..'),
(34, 26, 10, 1, 15000.00, 'ABB Устр-во автомат. регулирования ACS55-01E-02A2-2 0,37 кВт 220 В 1 фаза IP20 с фильтром ЭМС '),
(35, 27, 10, 3, 15000.00, 'ABB Устр-во автомат. регулирования ACS55-01E-02A2-2 0,37 кВт 220 В 1 фаза IP20 с фильтром ЭМС '),
(36, 28, 1, 1, 302860.69, 'M2AA 180 MLA IE1, 1500о/м, IMB35 на лапах'),
(37, 29, 3, 1, 120345.00, '1LF7106-2AB 2870 об\\мин'),
(38, 30, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(39, 31, 5, 1, 45567.00, 'DMR 132 KN'),
(40, 32, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(41, 33, 14, 1, 1810.00, 'Солнечный модуль FSM 30M'),
(42, 34, 4, 1, 140540.00, '1LF7096-4AB 2820 об\\мин'),
(43, 34, 10, 2, 15000.00, 'ABB Устр-во автомат. регулирования ACS55-01E-02A2-2 0,37 кВт 220 В 1 фаза IP20 с фильтром ЭМС '),
(44, 35, 9, 1, 430300.00, 'Преобразователь частоты ACS880-01-246A-3+E200 IP21 132 кВт ЕМС-фильтр лаковое покрытие плат');

-- --------------------------------------------------------

--
-- Структура таблицы `panels`
--

CREATE TABLE `panels` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `panel_product_id` mediumint(8) UNSIGNED NOT NULL,
  `panel_brand_id` tinyint(3) UNSIGNED NOT NULL,
  `panel_voltage` float(5,2) UNSIGNED NOT NULL,
  `panel_power` float(5,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `panels`
--

INSERT INTO `panels` (`id`, `panel_product_id`, `panel_brand_id`, `panel_voltage`, `panel_power`) VALUES
(1, 13, 4, 12.00, 50.00),
(2, 14, 4, 12.00, 30.00),
(3, 15, 5, 30.00, 250.00),
(4, 16, 5, 30.00, 300.00);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `product_category` tinyint(2) UNSIGNED NOT NULL,
  `product_alias` varchar(255) NOT NULL,
  `product_price` float(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `product_status` enum('1','0') NOT NULL DEFAULT '1',
  `product_desc` varchar(255) NOT NULL,
  `product_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `product_category`, `product_alias`, `product_price`, `product_status`, `product_desc`, `product_img`) VALUES
(1, 2, '2070348204', 302860.69, '1', 'M2AA 180 MLA IE1, 1500о/м, IMB35 на лапах', 'e7661c9e4f375a53c1216add409387b1.jpg'),
(2, 2, 'sgdfg5556367', 381777.00, '1', 'M2AA,IE1,37кВт,1000о/м,IMB3 /1шт./ 3GAA253041-ADG0..', 'e7661c9e4f375a53c1216add409387b1.jpg'),
(3, 2, 'cvbyhk', 120345.00, '1', '1LF7106-2AB 2870 об\\мин', '43993.jpg'),
(4, 2, '170964fb', 140540.00, '1', '1LF7096-4AB 2820 об\\мин', '43993.jpg'),
(5, 2, '456333', 45567.00, '1', 'DMR 132 KN', '144.png'),
(6, 2, '0877766', 22356.00, '1', 'DMR 160 SN', '144.png'),
(7, 2, 'ghdll99dsjhfff', 34123.00, '0', '1LA7060-4AB1 (0.12кВт/1500)', '143.png'),
(8, 2, '657898444', 56668.00, '0', '1LA7050-4AB1 1500 об/мин лапы B3', '143.png'),
(9, 3, '3awerrr4500033', 430300.00, '1', 'Преобразователь частоты ACS880-01-246A-3+E200 IP21 132 кВт ЕМС-фильтр лаковое покрытие плат', 'small_acs880-01_frame_size_r8_right_.jpg'),
(10, 3, '3aua1233244444', 15000.00, '1', 'ABB Устр-во автомат. регулирования ACS55-01E-02A2-2 0,37 кВт 220 В 1 фаза IP20 с фильтром ЭМС ', 'b0d269aea50237c55860817df2ac2b0e.jpg'),
(11, 3, '485776843', 81726.00, '1', 'Преобразователь частоты SINAMICS G120 силовой модуль PM230 без фильтра IP20 3AC380-480В 5.5кВт', 'small_p_d011_xx_00376i.jpg'),
(12, 3, '1237643', 30673.00, '1', 'Преобразователь частоты SINAMICS V20 1AC200-240В -100 47-63Гц 0.75кВт (6SL3210-5BB17-5UV0)', 'small_p_d011_xx_00397i.jpg'),
(13, 1, 'fsm50p', 3100.00, '0', 'Sunways ФСМ-50П', '1c70b26bcd7e1b28d5bf4f869d3732b9.jpg'),
(14, 1, 'fsm30p', 1810.00, '1', 'Солнечный модуль FSM 30M', 'd2c86e173b68e6aa10968b64a20da5a4.jpg'),
(15, 1, 'ssfsf12344', 6900.00, '0', 'Монокристаллические солнечные панели Aurinko 250 вт', 'b93730c232079cf7bf09b89d51a492e6.jpg'),
(16, 1, 'wetrtdfw233', 9500.00, '1', 'Солнечные панели DELTA являются фотоэлектрическими модулями', '96b9916024d3e0ef8c65e6eeb20f86c9.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `shippings`
--

CREATE TABLE `shippings` (
  `id` int(10) UNSIGNED NOT NULL,
  `shipping_user_id` mediumint(8) UNSIGNED NOT NULL,
  `shipping_note` varchar(255) DEFAULT NULL,
  `shipping_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `shipping_status` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shippings`
--

INSERT INTO `shippings` (`id`, `shipping_user_id`, `shipping_note`, `shipping_date`, `shipping_status`) VALUES
(2, 13, NULL, '2019-02-26 09:21:39', '0'),
(3, 13, NULL, '2019-02-26 09:26:08', '0'),
(4, 13, NULL, '2019-02-26 09:27:10', '0'),
(5, 13, NULL, '2019-02-26 09:34:58', '0'),
(6, 13, NULL, '2019-02-26 09:36:10', '0'),
(7, 13, NULL, '2019-02-26 09:40:01', '0'),
(8, 13, NULL, '2019-02-26 09:40:10', '0'),
(9, 13, NULL, '2019-02-26 09:51:18', '0'),
(10, 13, NULL, '2019-02-26 09:52:08', '0'),
(11, 13, NULL, '2019-02-26 09:55:15', '0'),
(12, 13, NULL, '2019-02-26 09:57:13', '0'),
(13, 13, NULL, '2019-02-26 10:02:50', '0'),
(14, 13, NULL, '2019-02-26 10:07:54', '0'),
(15, 13, NULL, '2019-02-26 10:09:40', '0'),
(16, 13, NULL, '2019-02-26 10:25:30', '0'),
(17, 13, NULL, '2019-02-26 10:27:17', '0'),
(18, 13, NULL, '2019-02-26 10:31:37', '0'),
(19, 13, NULL, '2019-02-26 10:34:07', '0'),
(20, 13, NULL, '2019-02-26 10:35:52', '0'),
(21, 13, NULL, '2019-02-26 10:40:36', '0'),
(22, 13, NULL, '2019-02-26 10:42:22', '0'),
(23, 13, NULL, '2019-02-26 10:42:35', '0'),
(24, 13, NULL, '2019-02-26 10:43:45', '0'),
(25, 15, NULL, '2019-02-26 10:45:08', '0'),
(26, 15, NULL, '2019-02-26 10:45:25', '0'),
(27, 15, NULL, '2019-02-26 10:46:59', '0'),
(28, 15, NULL, '2019-02-26 12:12:19', '0'),
(29, 15, NULL, '2019-02-26 12:12:49', '0'),
(30, 15, NULL, '2019-02-26 12:13:56', '0'),
(31, 15, NULL, '2019-02-26 12:15:51', '0'),
(32, 15, NULL, '2019-02-26 12:16:48', '0'),
(33, 15, NULL, '2019-02-26 12:21:51', '0'),
(34, 16, NULL, '2019-02-26 14:06:01', '0'),
(35, 16, NULL, '2019-02-26 14:11:16', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `user_login` varchar(255) NOT NULL,
  `user_password` char(64) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_role` enum('user','admin') NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `user_login`, `user_password`, `user_email`, `user_name`, `user_role`) VALUES
(13, 'sasha', '$2y$10$3m2WMSMLp9EQDU0KbAbAQO8.Ik1hsaM9zMm/SPisiE.hdQKMCD9VG', 'sasha@ru.com', 'sasha', 'user'),
(16, 'charlie', '$2y$10$1sJoiKhzJJZf4sq5FDzqve1UFH2exeWX2PxsF2SgAFwMbO1HD5FEW', 'Charlie61@yandex.ru', 'Павел', 'user');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `connects`
--
ALTER TABLE `connects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `connect_user_id` (`connect_user_id`);

--
-- Индексы таблицы `drives`
--
ALTER TABLE `drives`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drive_brand` (`drive_brand_id`),
  ADD KEY `drive_product_id` (`drive_product_id`);

--
-- Индексы таблицы `motors`
--
ALTER TABLE `motors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `motor_brand` (`motor_brand_id`),
  ADD KEY `motor_product_id` (`motor_product_id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_shipping_id` (`order_shipping_id`),
  ADD KEY `oreder_product_id` (`order_product_id`);

--
-- Индексы таблицы `panels`
--
ALTER TABLE `panels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `panel_brand` (`panel_brand_id`),
  ADD KEY `panel_product_id` (`panel_product_id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_category` (`product_category`);

--
-- Индексы таблицы `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shipping_user_id` (`shipping_user_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `brand`
--
ALTER TABLE `brand`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `connects`
--
ALTER TABLE `connects`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `drives`
--
ALTER TABLE `drives`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `motors`
--
ALTER TABLE `motors`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT для таблицы `panels`
--
ALTER TABLE `panels`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
