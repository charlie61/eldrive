'use strict';

var gulp = require('gulp'),
    prefixer = require('gulp-autoprefixer'),
    cssmin = require('gulp-cssmin'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    rigger = require('gulp-rigger'),
    uglify = require('gulp-uglify');

let path = {
    pub:{
        all:'public/',
        html:'public/',
        js:'public/js/',
        css:'public/css/',
        img:'public/img/',
        fonts:'public/fonts'
    },
    src:{
        html:'dev/**/*.html',
        js:'dev/js/**/*.js',
        img:'dev/img/**/*.*',
        css:'dev/css/**/*.css',
        fonts:'dev/fonts/**/*.*'
    },
},
    config = {
        server:{
            baseDir:"public"
        },
        tunnel:true,
        host:'localhost',
        port:7787,
        logPrefix:'Pavel',
        index:'index.html'
    };
gulp.task('html', function(done){
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.pub.html))
        .pipe(reload({stream: true}));
    done();
});
gulp.task('css', function(done){
    gulp.src(path.src.css)
        .pipe(prefixer({
            cascade: true,
            browsers: ['last 5 versions'],
            remove: true
        }))
        .pipe(cssmin())
        .pipe(gulp.dest(path.pub.css))
        .pipe(reload({stream: true}));
    done();
});
gulp.task('js', function(done){
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.pub.js))
        .pipe(reload({stream: true}));
    done();
});
gulp.task('img', function(done){
   gulp.src(path.src.img)
       .pipe(gulp.dest(path.pub.img))
       .pipe(reload({stream: true}));
   done();
});

gulp.task('watch', function(){
    gulp.watch(path.src.html, gulp.series('html'));
    gulp.watch(path.src.css, gulp.series('css'));
    gulp.watch(path.src.js, gulp.series('js'));
    gulp.watch(path.src.img, gulp.series('img'));
});
gulp.task('server', function(done){
   browserSync(config);
   done();
});

gulp.task('build', gulp.parallel('html', 'css', 'js', 'img'));
gulp.task('default', gulp.series('build', 'server', 'watch'));