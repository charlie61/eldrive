jQuery(document).ready(function($){
    var products = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            wildcard: '%QUERY',
            url: path + '/search/typeahead?query=%QUERY'
        }
    });

    products.initialize();

    $("#typeahead").typeahead({
        // hint: false,
        highlight: true
    },{
        name: 'products',
        display: 'title',
        limit: 10,
        source: products
    });

    $('#typeahead').bind('typeahead:select', function(ev, suggestion) {
        // console.log(suggestion);
        window.location = path + '/search/?s=' + encodeURIComponent(suggestion.title);
    });

    AOS.init();
    var siteCarousel = function () {
        if ( $('.slide-one-item').length > 0 ) {
            $('.slide-one-item').owlCarousel({
                center: false,
                items: 1,
                loop: true,
                stagePadding: 0,
                margin: 0,
                autoplay: false,
                pauseOnHover: false,
                nav: false
            });
        }
    };
    siteCarousel();
    var siteStellar = function() {
        $(window).stellar({
            responsive: false,
            parallaxBackgrounds: true,
            parallaxElements: true,
            horizontalScrolling: false,
            hideDistantElements: false,
            scrollProperty: 'scroll'
        });
    };
    siteStellar();

    $('body').on('click', '#add-to-cart', function(e){
        e.preventDefault();
        let id = $(this).data('id'),
            qty = $('#quantity').val()? $('#quantity').val():1;

        $.ajax({
            url:'/cart/add',
            data: {id: id, qty: qty},
            type: 'GET',
            success: function(res){
                showCart(res);
            },
            error: function(){
                alert('Ошибка! Попробуйте позже!');
            }
        });

    });
    function showCart(cart){
        $('#cart_info').html(cart);
    }

    function showFleshCart(cart){
        $('#cart-controller').html(cart);
        showUpdated();
    }
    function showUpdated(){
        var qty = $('.cart-qty').html();
        if(qty){
            $('#cart_info').html(qty);
        }else{
            $('#cart_info').html('');
        }
    }

    $('body').on('click', '.del-item', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        $.ajax({
           url:'/cart/delete',
            data: {id: id},
            type: 'GET',
            success: function(res){
                showFleshCart(res);
            },
            error: function(){
                alert('Ошибка! Попробуйте позже!');
            }
        });
    });

    $('body').on('click', '#clearAll', function(e){
        e.preventDefault();
        $.ajax({
            url:'/cart/clear',
            type: 'GET',
            success: function(res){
                showFleshCart(res);
            },
            error: function(){
                alert('Ошибка! Попробуйте позже')
            }
        });

    });

    $('body').on('click', '#purchase', function(e){
        e.preventDefault();
        $.ajax({
            url: '/cart/purchase',
            type: 'GET',
            success: function(res){
                showFleshCart(res);
                console.log('Hi!');
            },
            error: function(){
                alert('Ошибка! попробуйте позже!')
            }
        })
    });



});

(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();